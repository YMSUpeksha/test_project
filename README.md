# Test project

test project is a training application developed to master basics of git commands and java development.

## Installation

Use the following command to clone test project to a local machine.

```bash
git clone https://gitlab.com/YMSUpeksha/test_project.git
```

## Pre-requisites


IDE - preferably IntelliJ IDEA, Eclipse | netbeans\
[JRE - ](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) Java runtime environment according to the operating system.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
TDD recommended. 

